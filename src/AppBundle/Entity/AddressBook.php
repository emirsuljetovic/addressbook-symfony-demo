<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/** Class AddressBook
*@package AppBundle\Entity
*
*@ORM\Entity
*@ORM\Table(name="contacts")
*/

class AddressBook {
    /** @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /** @ORM\Column(type="string") */
    private $firstName;

    /** @ORM\Column(type="string") */
    private $lastName;

    /** @ORM\Column(type="string") */
    private $streetAndNumber;

    /** @ORM\Column(type="string") */
    private $zip;

    /** @ORM\Column(type="string") */
    private $city;

    /** @ORM\Column(type="string") */
    private $country;

    /** @ORM\Column(type="text") */
    private $phoneNumber;

    /** @ORM\Column(type="text") */
    private $birthday;

    /** @ORM\Column(type="string") */
    private $emailAddress;

    /** @ORM\Column(type="string", nullable=true) */
    private $imageUpload;

    public function __construct($data) {
        $this->firstName = $data['firstName'];
        $this->lastName = $data['lastName'];
        $this->streetAndNumber = $data['streetAndNumber'];
        $this->zip = $data['zip'];
        $this->city = $data['city'];
        $this->country = $data['country'];
        $this->phoneNumber = $data['phoneNumber'];
        $this->birthday = $data['birthday'];
        $this->emailAddress = $data['emailAddress'];
        $this->imageUpload = $data['imageUpload'];
    }

    public function getId() {
        return $this->id;
    }
    public function getFirstName() {
        return $this->firstName;
    }
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }
    public function getLastName() {
        return $this->lastName;
    }
    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }
    public function getStreetAndNumber() {
        return $this->streetAndNumber;
    }
    public function setStreetAndNumber($streetAndNumber) {
        $this->streetAndNumber = $streetAndNumber;
    }
    public function getZip() {
        return $this->zip;
    }
    public function setZip($zip) {
        $this->zip = $zip;
    }
    public function getCity() {
        return $this->city;
    }
    public function setCity($city) {
        $this->city = $city;
    }
    public function getCountry() {
        return $this->country;
    }
    public function setCountry($country) {
        $this->country = $country;
    }
    public function getPhoneNumber() {
        return $this->phoneNumber;
    }
    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }
    public function getBirthday() {
        return $this->birthday;
    }
    public function setBirthday($birthday) {
        $this->birthday = $birthday;
    }
    public function getEmailAddress() {
        return $this->emailAddress;
    }
    public function setEmailAddress($emailAddress) {
        $this->emailAddress = $emailAddress;
    }
    public function getImageUpload() {
        return $this->imageUpload;
    }
    public function setImageUpload($imageUpload) {
        $this->imageUpload = $imageUpload;
    }
    
    
}