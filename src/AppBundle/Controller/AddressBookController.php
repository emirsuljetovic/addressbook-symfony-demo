<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AddressBook;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AddressBookController extends AbstractController
{
     
    /**
     * @Route("/add", name="addressbook")
     */  
    public function new(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('streetAndNumber', TextType::class)
            ->add('zip', TextType::class)
            ->add('city', TextType::class)
            ->add('country', TextType::class)
            ->add('phoneNumber', TextType::class)
            ->add('birthday', TextType::class, array(
                'attr' => array('autocomplete' => 'off')
            ))
            ->add('emailAddress', EmailType::class)
            ->add('imageUpload', FileType::class, array(
                'data_class'=> null,
                'label' => 'Upload new image',
                'required' => false
            ))
            ->add('save', SubmitType::class, array('label' => 'Add Contact'))
            ->getForm();

            $form->handleRequest($request);
            
            if ($form->isSubmitted() && $form->isValid()) {

                $data = $form->getData();             
                $entityManager = $this->getDoctrine()->getManager();

                $addressBook = new AddressBook($data);
                $entityManager->persist($addressBook);
                $entityManager->flush();
                
                return $this->redirectToRoute('details', array('id' => $addressBook->getId()));
            }
        return $this->render('default/AddressBook/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

   

}