<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\AddressBook;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function listAction(Request $request)
    {
        $contacts = $this->getDoctrine()->getRepository('AppBundle:AddressBook')->findAll();

        return $this->render('default/AddressBook/index.html.twig', [
            'contacts' => $contacts
        ]);
    }

    /**
     * @Route("/details/{id}", name="details")
     */
     public function detailsAction($id)
     {
        $entityManager = $this->getDoctrine()->getManager();
        $imagesDir = $this->get('kernel')->getRootDir() . 'uploads/';

        $contact = $this->getDoctrine()->getRepository('AppBundle:AddressBook')->findOneBy([
            'id' => $id
        ]);

         return $this->render('default/AddressBook/details.html.twig', [
            'contact' => $contact,
            'imagesDir' => $imagesDir
        ]);
     }

     /**
     * @Route("/remove/{id}", name="removecontact")
     */
     public function removeAction($id)
     {
        $entityManager = $this->getDoctrine()->getManager();
        $contact = $entityManager->getRepository(AddressBook::class)->find($id);

        $entityManager->remove($contact);
        $entityManager->flush();

         return $this->redirectToRoute('homepage');
     }

     /**
     * @Route("/edit/{id}", name="editcontact")
     */
     public function editAction(Request $request, $id)
     {
         $entityManager = $this->getDoctrine()->getManager();
         $contact = $entityManager->getRepository(AddressBook::class)->find($id);
         /** @var $post AddressBook */

         $form = $this->createFormBuilder()
         ->add('firstName', TextType::class, array(
             'data' => $contact->getFirstName()
         ))
         ->add('lastName', TextType::class, array(
            'data' => $contact->getLastName()
        ))
         ->add('streetAndNumber', TextType::class, array(
            'data' => $contact->getStreetAndNumber()
        ))
         ->add('zip', TextType::class, array(
            'data' => $contact->getZip()
        ))
         ->add('city', TextType::class, array(
            'data' => $contact->getCity()
        ))
         ->add('country', TextType::class, array(
            'data' => $contact->getCountry()
        ))
         ->add('phoneNumber', TextType::class, array(
            'data' => $contact->getPhoneNumber()
        ))
         ->add('birthday', TextType::class, array(
            'attr' => array('autocomplete' => 'off'),
            'data' => $contact->getBirthday()
        ))
         ->add('emailAddress', EmailType::class, array(
            'data' => $contact->getEmailAddress()
        ))
         ->add('imageUpload', FileType::class, array(
            'data_class'=> null,
            'label' => 'Upload new image',
            'required' => false,
            'attr' => array('autocomplete' => 'off'),
            'data' => $contact->getImageUpload()
        ))
         ->add('save', SubmitType::class, array('label' => 'Update Contact'))
         ->getForm();
     
         $form->handleRequest($request);
            
            if ($form->isSubmitted() && $form->isValid()) {

                $data = $form->getData();
                $firstName = $data['firstName'];
                $lastName = $data['lastName'];
                $streetAndNumber = $data['streetAndNumber'];
                $zip = $data['zip'];
                $city = $data['city'];
                $country = $data['country'];
                $phoneNumber = $data['phoneNumber'];
                $birthday = $data['birthday'];
                $emailAddress = $data['emailAddress'];
                $imageUpload = $data['imageUpload'];

                $contact->setFirstName($firstName);
                $contact->setLastName($lastName);
                $contact->setStreetAndNumber($streetAndNumber);
                $contact->setZip($zip);
                $contact->setCity($city);
                $contact->setCountry($country);
                $contact->setPhoneNumber($phoneNumber);
                $contact->setBirthday($birthday);
                $contact->setEmailAddress($emailAddress);
                $contact->setImageUpload($imageUpload);

                $entityManager->flush();
                
                return $this->redirectToRoute('details', array('id' => $id));
            }

         return $this->render('default/AddressBook/edit.html.twig', array(
            'form' => $form->createView(),
        ));
     }
}